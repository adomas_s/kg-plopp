from flask import Flask, render_template, send_from_directory, redirect, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from collections import defaultdict
import datetime

app = Flask(__name__, static_url_path="")
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://localhost/kg-plopp'
db = SQLAlchemy(app)


@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory("assets/js", path)


@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory("assets/css", path)


class Decibel(db.Model):
    __tablename__ = "decibels"
    id = db.Column(db.Integer, primary_key=True)
    microphone_id = db.Column(db.String)
    decibel = db.Column(db.Integer)
    time = db.Column(db.DateTime)

    def __init__(self, microphone_id, decibel, time):
        self.microphone_id = microphone_id
        self.decibel = decibel
        self.time = time

    def __repr__(self):
        return "Decibel({}, {}, {})".format(self.microphone_id, self.decibel, self.time)


@app.route("/")
def hello_world():
    return render_template("register.html")


@app.route("/audience")
def dashboardView():
    return render_template("dashboard.html")


@app.route("/register_mic", methods=["GET"])
def register_mic():
    return redirect("/record/" + request.args.get("mic_id"))


@app.route("/register_volume/<string:mic_id>/<int:decibel_value>", methods=["POST"])
def register_volume(mic_id, decibel_value):
    decibel = Decibel(mic_id, decibel_value, datetime.datetime.now())
    db.session.add(decibel)
    db.session.commit()
    return str(decibel)


@app.route("/reload_dashboard/<int:seconds>", methods=["GET"])
def reload_dashboard(seconds):
    x_seconds_ago = datetime.datetime.now() - datetime.timedelta(seconds=seconds)
    decibels = Decibel.query.filter(Decibel.time > x_seconds_ago).all()

    response = defaultdict(list)

    for dec in decibels:
        entry = {"y": dec.decibel}
        response[dec.microphone_id].append(entry)

    for mic_id in response:
        mic_entries = len(response[mic_id])
        if mic_entries < seconds:
            missing_entry_count = seconds - mic_entries
            for i in range(missing_entry_count):
                response[mic_id].append({"y": 0})


    return jsonify(response)


@app.route("/record/<string:mic_id>", methods=["GET"])
def record(mic_id):
    return render_template("record.html")

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0')
