# KG-Plopp

## Installation
Install Python 3:
`brew install python3`

Install virtualenv with pip:
`pip install virtualenv`

Create virtual environment:
`virtualenv env`

And activate it:
`source env/bin/activate`

Install prerequisites:
`pip install -r requirements.txt`

## Running
Run:
`python app.py`


## Database
You need to set up a Postgres database called `kg-plopp`:
`createdb kg-plopp`

To set up the table, run
```
$ python
>>> from app import db
>>> db.create_all()
>>> exit()
```

## Client
Start the server:
`cd templates && python -m SimpleHTTPServer 8080`

Now open your browser in http://localhost:8080/index.html.
