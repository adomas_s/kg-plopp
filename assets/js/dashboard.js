DashboardApp = (function() {
  var loudnessThreshold = 220;
  var mikes = {};
  var mikeContainer = document.getElementById("mikeContainer");
  var refreshLoop;
  var refreshFrequency = 500;
  var dataPoints = {};

  function start() {
    getData();
    setInterval(getData, refreshFrequency);
  }

  function updateColor(loudnessLevel) {
    var body = document.getElementsByTagName('body')[0];

    if(loudnessLevel > loudnessThreshold) {
      body.classList.add("loud");
    } else {
      body.classList.remove("loud");
    }
  }

  // TODO: normalize name to alphanumeric chars, so we can use it as id
  function elementId(mic_id) {
    return "chart-" + mic_id;
  }

  function addMicrophone(mic_id) {
    var micChart = document.createElement('div');

    micChart.setAttribute("id", elementId(mic_id));
    micChart.setAttribute("class", "mic-chart");

    this.mikeContainer.append(micChart);

    mikes[mic_id] = true;
  }

  function getData() {
    var r = new XMLHttpRequest();
    var data;
    r.open("GET", "/reload_dashboard/60", true);
    r.onreadystatechange = function () {
      if (r.readyState != 4 || r.status != 200) return;
      updateDashboard(JSON.parse(r.responseText));
    };
    r.send();
  }

  function updateDashboard(data) {
    for (var mic_id in data) {
      dataPoints[mic_id] = data[mic_id];
      if (!mikes[mic_id]) addMicrophone(mic_id);
      updateMicChart(mic_id)
    }
  }

  function updateMicChart(mic_id) {
    var chart = new CanvasJS.Chart(elementId(mic_id), {
      title: {
        text: mic_id
      },
      axisY: {
        title: "Loudness",
        minimum: 0,
        maximum: 256
      },
      data: [{
        type: "line",
        dataPoints: dataPoints[mic_id]
      }]
    });
    chart.render();
  }

  return {
    start: start,
    updateColor: updateColor
  }
})();

document.addEventListener("DOMContentLoaded", function() {
  DashboardApp.start();
});
