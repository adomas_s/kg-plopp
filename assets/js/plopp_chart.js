PloppChart = (function() {
  var dataPoints = [];
  var chart;
  var lastX = 0;
  var maxDataPoints = 50; // number of dataPoints visible at any point

  function start() {
    for (var i = 0; i < maxDataPoints; i++) {
      dataPoints.push({ x: lastX, y: 0 });
      lastX = lastX + 1;
    }
    chart = new CanvasJS.Chart("chartContainer", {
      title :{
        text: window.location.href.split("/").pop()
      },
      axisY: {
        title: "Loudness",
        minimum: 0,
        maximum: 256
      },
      data: [{
        type: "line",
        dataPoints: dataPoints
      }]
    });
  }

  function updateChart() {
    if (dataPoints.length > maxDataPoints) {
      dataPoints.shift();
    }
    chart.render();
  }

  function addPoint(value) {
    dataPoints.push({ x: lastX, y: value });
    lastX = lastX + 1;
    updateChart();
  }

  return {
    start: start,
    addPoint: addPoint
  }
})();

document.addEventListener("DOMContentLoaded", function() {
  PloppChart.start();
});
