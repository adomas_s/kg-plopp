PloppApp = (function() {
  var publishFrequency = 100;
  var backendFrequency = 1000;
  var audioContext = new (window.AudioContext || webkitAudioContext)();
  var loudnessBuffer = [];
  var backendBuffer = [];
  var mediaRecorder;
  var publishingLoop;
  var registeringLoop;

  function start() {
    if (!navigator.mediaDevices) return onError("Cannot use media devices");

    navigator.mediaDevices
      .getUserMedia({ audio: true })
      .then(record, onError);

    publishingLoop = setInterval(publish, publishFrequency);
    registeringLoop = setInterval(registerInBackend, backendFrequency);
  }

  function monitor(stream) {
    var source = audioContext.createMediaStreamSource(stream);

    var analyser = audioContext.createAnalyser();
    analyser.fftSize = 2048;
    var bufferLength = analyser.frequencyBinCount;
    var dataArray = new Uint8Array(bufferLength);
    var dataArray = dataArray.map(function(value) {
      return Math.abs(value - 127);
    });

    source.connect(analyser);

    populateLoudnessBuffer();

    function populateLoudnessBuffer() {
      requestAnimationFrame(populateLoudnessBuffer);
      analyser.getByteTimeDomainData(dataArray);
      var max = Math.max.apply(null, dataArray);
      var min = Math.min.apply(null, dataArray);
      var diff = (max - min);
      loudnessBuffer.push(diff);
    }
  }

  function record(stream) {
    mediaRecorder = new MediaRecorder(stream);
    monitor(stream);
  }

  function registerInBackend() {
    if (!backendBuffer.length) return;
    var maxValue = Math.max.apply(null, backendBuffer);
    clearBackendBuffer();
    registerVolume(maxValue);
  }

  function publish() {
    if (!loudnessBuffer.length) return;

    var value = maxLoudness();
    clearLoudnessBuffer();

    if (typeof DashboardApp !== "undefined") DashboardApp.updateColor(value);
    if (typeof PloppChart !== "undefined") PloppChart.addPoint(value);
    backendBuffer.push(value);
  }

  function maxLoudness() {
    return Math.max.apply(null, loudnessBuffer);
  }

  function averageLoudness() {
    return parseInt(loudnessBuffer.reduce(function(a, b) {
        return a + b;
      }) / loudnessBuffer.length);
  }

  function registerVolume(value) {
    var request = new XMLHttpRequest();
    var url = "/register_volume/" + micId() + "/" + value.toString();
    request.open("POST", url, true);
    request.send();
  }

  function clearLoudnessBuffer() {
    loudnessBuffer = [];
  }

  function clearBackendBuffer() {
    backendBuffer = [];
  }

  function stop() {
    publishingLoop = null;
  }

  function onError(error) {
    console.warn("Error: " + error);
  }

  function micId() {
    return window.location.href.split("/").pop();
  }

  return {
    start: start,
    stop: stop
  };
})();

document.addEventListener("DOMContentLoaded", function() {
  PloppApp.start();
});
